# Bienvenue dans le README de Bikeeshop !


# Les prérequis pour faire tourner l'application web Bikeeshop :

- Un serveur web (Apache)
- Java 1.8 : (JRE + JDK version 8)
- Un serveur de base de données MySQL
- Un terminal pour exécuter des commandes ou un EDI Java (IntelliJ, Eclipse)
- Maven (Seulement si jamais vous souhaitez utiliser la ligne de commande) (Car il est embarqué par défaut dans la plupart des EDI)


# Pour configurer la base de données MySQL :

- Importer dans votre SGBD MySQL le script de base de données "database/bikeeshop.sql"
- Par défaut l'application tentera une connection a la base de données avec les credentials suivant : 
	- user: bikeeshop
	- password: gk5mCn9JteAnrk9T
- Si besoin, modifiez le fichier web-services/src/main/resources/application.properties afin de faire correspondre les paramètres de connexion à la base de données avec votre configuration.


# Pour lancer le web-service :

- Ouvrir un terminal et se rendre sur le dossier web-services/
- Exécuter la commande mvn spring-boot:run et attendre le lancement de l'application
- De manière alternative, vous pouvez aussi utiliser un EDI Java et exécuter la classe ServerApplication.java


# Pour configurer et consulter le site web :

- Importer dans votre dossier htdocs ou www de votre serveur web le dossier nommé "client"
- Se rendre sur l'url vous permettant d'accéder au site
- Se créer un compte
- Une fois connecté, vous serez redirigé vers l'accueil du site qui vous guidera vers les différents écrans que propose le site.