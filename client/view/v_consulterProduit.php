<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title"><span class="glyphicon glyphicon-file"></span> <?php echo $produit['name'] ?> !</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $produit['imagesLink'][0] ?>"/>
                            </div>
                            <div class="col-md-6">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Caractéristique
                                            </th>
                                            <th>
                                                Valeur
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Nom</td>
                                            <td><?php echo $produit['name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><?php echo $produit['description'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Prix</td>
                                            <td><?php echo $produit['prix'] ?> €</td>
                                        </tr>
                                        <tr>
                                            <td>Stock</td>
                                            <td><?php echo $produit['stock'] ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <form method="POST" action="index.php?uc=basket&action=ajouterPanier&idProduit=<?php echo $produit['id'] ?>">
                                    <div class="col-md-6">
                                        <input class="form-control" placeholder="Quantité" id="quantiteProduit" name="quantiteProduit" type="text" value="1">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Ajouter au panier</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

