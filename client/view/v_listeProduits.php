<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title"><span class="glyphicon glyphicon-star"></span> Vélos de la catégorie <?php echo $nameCategorie ?> !</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="row">
                            
                        <?php 
                            foreach ($produits as $produit) {
                                echo '<div class="col-md-3">
                                    <a href="index.php?uc=product&action=consulterProduit&idProduit='.$produit['id'].'" class="thumbnail" data-gallery="gallery1">
                                        <img src="'.$produit['imagesLink'][0].'" alt="'.$produit['name'].'" />
                                    </a>
                                </div>';
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>