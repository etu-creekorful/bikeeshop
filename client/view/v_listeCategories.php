
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title"><span class="glyphicon glyphicon-th-large"></span> Categories de vélos</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <?php 
                            foreach ($categories as $categorie) {
                                echo '<a href="index.php?uc=product&action=voirListeProduits&idCategorie='.$categorie['id'].'&nameCategorie='.$categorie['name'].'"><button class="btn btn-success btn-lg">' . $categorie['name'] . '</button></a> &nbsp;';
                            }
                        ?>
                        <a href="index.php?uc=product&action=voirListeProduits&idCategorie=000&nameCategorie=tous"><button class="btn btn-success btn-lg"> Tous </button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title"><span class="glyphicon glyphicon-star"></span> Nos top vélos !</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="row">
                            
                        <?php 
                            foreach ($topProducts as $topProduct) {
                                echo '<div class="col-md-3">
                                    <a href="index.php?uc=product&action=consulterProduit&idProduit='.$topProduct['id'].'" class="thumbnail" data-gallery="gallery1">
                                        <img src="'.$topProduct['imagesLink'][0].'" alt="'.$topProduct['name'].'" />
                                    </a>
                                </div>';
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>