<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <p>Votre quantité est invalide ! Vous n'avez saisi aucune quantité ou une quantité supérieure au stock</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <a href="index.php?uc=product&action=consulterProduit&idProduit=<?php echo $produit['id'] ?>">
            <button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Revenir au produit</button>
        </a>
    </div>
</div>