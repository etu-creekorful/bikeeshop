<div class="row">
    <div class="col-md-12">
        <h2>
            <center>Voici vos commandes</center>
        </h2>
    </div>
</div>
<br/>

<?php
if(count($commandes) > 0) {
?>

<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Date de commande</th>   
                    <th>Contenu de la commande</th>
                    <th>Total de la commande</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($commandes as $commande) {
                    echo '<tr>';
                    echo '<td>' . date("j/m/Y g:i:s", substr($commande['orderDate'], 0, 10)) . '</td>'; // Convert from epoch time
                    echo '<td><ul>';
                    //Pour chaque produit de notre commande
                    $totalCommande = 0;
                    foreach ($commande['products'] as $produit) {
                        $totalCommande += (float) $produit['unitPrice'] * $produit['quantity'];
                        echo '<li>';
                        echo '<a href="index.php?uc=product&action=consulterProduit&idProduit='.$produit['productId'].'">'
                                . 'Produit ' . getProductById($produit['productId'])['name'] . ' (Quantité commandée: ' . $produit['quantity'] . ', Prix unitaire: ' . $produit['unitPrice'] .' €)'
                                . '</a>';
                        echo '</li>';
                    }
                    echo '</ul></td>';
                    echo '<td><strong>' . $totalCommande . ' €</strong></td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<?php
} else {
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <p>Vous n'avez aucune commande</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <a href="index.php?uc=productCategories&action=voirCategories">
            <button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Revenir aux catégories</button>
        </a>
    </div>
</div>

<?php } ?>
