<div class="row">
    <div class="col-md-12">
        <h2>
            <center>Voici le contenu de votre panier</center>
        </h2>
    </div>
</div>
<br/>

<div class="row">
    <div class="col-md-12">
        
        <?php
        
        if(count($_SESSION['basket']) != 0) {

        ?>
        
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Nom du produit</th>                  
                    <th>Prix unitaire</th>               
                    <th>Quantité</th> 
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $prixTotal = 0;

                foreach ($produits as $produit) {
                    $prixTotal += (float) $produit['prix'] * $_SESSION['basket'][$produit['id']];
                    echo '<tr>';
                    echo '<td style="width:15%; height:15%;"><a href="index.php?uc=product&action=consulterProduit&idProduit='.$produit['id'].'" class="thumbnail" data-gallery="gallery1">
                                    <img src="'.$produit['imagesLink'][0].'" alt="'.$produit['name'].'" />
                                </a></td>';
                    echo '<td>' . $produit['name'] . '</td>';        
                    echo '<td>' . $produit['prix'] . ' €</td>';
                    echo '<td>' . $_SESSION['basket'][$produit['id']] . '</td>';
                    echo '<td><a href="index.php?uc=basket&action=supprimerPanier&idProduit='.$produit['id'].'"><button class="btn btn-danger">Supprimer</button></a></td>';
                    echo '</tr>';
                }

                echo '<tr><td></td><td></td><td></td><td><strong>Prix total: </strong></td><td><strong>'. $prixTotal .' €</strong></td></tr>';
                ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-2 col-md-offset-10">
        <a href="index.php?uc=basket&action=validerPanier">
            <button class="btn btn-success">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                Commander maintenant
            </button>
        </a>
    </div>
</div>
<br/>

<?php
} else {
    
?>
    
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <p>Vous n'avez aucun produit dans votre panier</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <a href="index.php?uc=productCategories&action=voirCategories">
            <button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Revenir aux catégories</button>
        </a>
    </div>
</div>

<?php
}
?>