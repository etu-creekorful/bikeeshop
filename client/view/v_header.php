<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="UTF-8" />
    <title>Bikeeshop</title> 
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src ="./ajax/js/_ajax.lib.js"></script>
</head>
<body>
    <div class="container">
        <?php
        if ($isConnected) {
            ?>
            <div class="header">
                <div class="row vertical-align">
                    <div class="col-md-4">
                        <h1><img src="./img/logo.jpg" class="img-responsive" alt="Bikeeshop" title="Bikeeshop"></h1>
                    </div>
                    <div class="col-md-8">
                        <ul class="nav nav-pills pull-right" role="tablist">
                            <li <?php if (!isset($_REQUEST['uc']) || $_REQUEST['uc'] == 'accueil') { ?> class="active"<?php } ?>><a href="index.php">Accueil</a></li>
                            <li <?php if (isset($_REQUEST['uc']) && $_REQUEST['uc'] == 'productCategories') { ?> class="active"<?php } ?>><a href="index.php?uc=productCategories&action=voirCategories">Catégories</a></li>
                            <li <?php if (isset($_REQUEST['uc']) && $_REQUEST['uc'] == 'basket') { ?> class="active"<?php } ?>><a href="index.php?uc=basket&action=consulterPanier">Panier</a></li>
                            <li <?php if (isset($_REQUEST['uc']) && $_REQUEST['uc'] == 'commandes') { ?> class="active"<?php } ?>><a href="index.php?uc=commandes&action=voirCommandes">Vos commandes</a></li>
                            <li <?php if (isset($_REQUEST['uc']) && $_REQUEST['uc'] == 'deconnexion') { ?> class="active"<?php } ?>><a href="index.php?uc=deconnection&action=demandeDeconnexion">Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php
        } else {
            ?>   
            <h1><img src="./img/logo.jpg" class="img-responsive center-block" alt="Laboratoire Galaxy-Swiss Bourdin" title="Laboratoire Galaxy-Swiss Bourdin"></h1>
        <?php
        }
        ?>
<script type="text/javascript" src="./ajax/js/jquery-1.11.3.min.js"></script>