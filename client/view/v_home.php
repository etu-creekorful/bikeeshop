﻿<div id="accueil">
    <h2>Bienvenue sur Bikeeshop</h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="glyphicon glyphicon-bookmark"></span> Navigation</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <a href="index.php?uc=productCategories&action=voirCategories" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-plus-sign"></span> <br/>Voir les catégories de produits</a>
                        <a href="index.php?uc=basket&action=consulterPanier" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> <br/>Consulter votre panier</a>
                        <a href="index.php?uc=commandes&action=voirCommandes" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Consulter vos commandes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>