<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Inscription utilisateur</h3>
            </div>
            <div class="panel-body">
                <form role="form" method="post" action="index.php?uc=inscription&action=valideInscription">
                    <fieldset>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <input class="form-control" placeholder="Nom" name="nom" type="text" maxlength="45" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <input class="form-control" placeholder="Prénom" name="prenom" type="text" maxlength="45" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input class="form-control" placeholder="Email" name="email" type="email" maxlength="45" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input class="form-control" placeholder="Mot de passe" name="mdp" type="password" maxlength="45" required>
                            </div>
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="S'enregistrer">
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<br/>

<div class="row">
    <div class="col-md-12">
        <center>
            <p><a href="index.php?uc=connection&action=demandeConnection">Retourner à la page de connexion</a></p>
        </center>
    </div>
</div>

