<?php

require_once("include/fct.inc.php");
session_start();
$isConnected = isConnected();
include("view/v_header.php");
if (!isset($_REQUEST['uc']) && !$isConnected) {
    $_REQUEST['uc'] = 'connection';
} else if (empty($_REQUEST['uc'])) {
    $_REQUEST['uc'] = 'home';
}
$uc = $_REQUEST['uc'];

switch ($uc) {
    case 'connection': {
            include("controller/c_connection.php");
            break;
    }
    case 'inscription': {
            include("controller/c_inscription.php");
            break;
    }
    case 'deconnection' : {
            include("controller/c_deconnexion.php");
            break;
    }
    case 'home': {
            include("controller/c_home.php");
            break;
    }
    case 'productCategories' : {
            include("controller/c_getCategories.php");
            break;
    }
    case 'commandes' : {
            include("controller/c_commandes.php");
            break;
    }
    case 'product' : {
            include("controller/c_getProducts.php");
            break;
    }
    case 'basket' : {
            include("controller/c_basket.php");
            break;
    }
}
include("view/v_footer.php");
?>