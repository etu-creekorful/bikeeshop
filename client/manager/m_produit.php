<?php

function getProductCategories() {
    return getAPI('categories');
}

function getTopProducts() {
    return getAPI('produits?onlyTopProduct=true');
}

function getAllProducts() {
    return getAPI('produits');
}

function getProductsByCategory($idCategory) {
    return getAPI('produits?categorie=' . $idCategory);
}

function getProductById($idProduct) {
    return getAPI('produits/' . $idProduct);
}