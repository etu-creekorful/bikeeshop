<?php

function createUser($data) {
    return postAPI('users', $data, false);
}

function connectUser($login, $mdp) {
    return authAPI($login, $mdp);
}
