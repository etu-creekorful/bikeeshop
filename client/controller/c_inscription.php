<?php

include('./manager/m_user.php');

$action = $_REQUEST['action'];

switch ($action) {
    case 'demandeInscription': {
        include("view/v_inscription.php");
        break;
    }
    
    case 'valideInscription': {
        $data = array(
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'email' => $_POST['email'],
            'password' => $_POST['mdp']
        );
        
        $result = createUser($data);
        
        $message = "";
        if($result === '') {
            $message = "Vous avez bien été inscrit, vous allez être redirigé vers la page de connexion";
        } else {
            $message = "Un problème est survenu lors de votre inscription, veuilez rééssayer ultérieurement";
        }
        
        include("view/v_actionValide.php");
        header("Refresh: 3;URL=index.php?uc=connection&action=demandeConnexion");
        
        break;
    }
}
