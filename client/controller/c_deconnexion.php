﻿<?php
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'demandeDeconnexion';
}
$action = $_REQUEST['action'];
switch ($action) {
    case 'demandeDeconnexion': {
            session_destroy();
            header('Location: index.php?uc=connection&action=demandeConnexion');
            break;
        }
}
?>