<?php

include('./manager/m_produit.php');

$action = $_REQUEST['action'];

switch($action) {
    case 'voirListeProduits': {
        $idCategorie = $_REQUEST['idCategorie'];        
        $nameCategorie = $_REQUEST['nameCategorie'];
        
        $produits = array();
        if($idCategorie != '000') {
            $produits = getProductsByCategory($idCategorie);
        } else {
            $produits = getAllProducts();
        }
        
        include('view/v_listeProduits.php');
        break;
    }
    case 'consulterProduit': {
        $idProduit = $_REQUEST['idProduit'];
        
        $produit = getProductById($idProduit);
        
        include('view/v_consulterProduit.php');
        break;
    }
}