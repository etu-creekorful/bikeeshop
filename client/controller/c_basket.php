<?php

include('./manager/m_produit.php');
include('./manager/m_commandes.php');

$action = $_REQUEST['action'];

switch($action) {
    case 'ajouterPanier': {
        $produit = getProductById($_GET['idProduit']);
        $quantiteCommande = $_POST['quantiteProduit'];
        
        //Si la quantité n'est pas vide et est bien conforme par rapport au stock du produit
        if($quantiteCommande != '' && $quantiteCommande <= $produit['stock']) {
            //On vérifie que le produit n'est pas déjà présent dans le panier,
            //Sinon on met à jour la quantité
            $_SESSION['basket'][$produit['id']] = $quantiteCommande;
                        
            //On redirige vers la consultation du panier
            header('Location: index.php?uc=basket&action=consulterPanier');          
        } else {
            include('view/v_erreurAjoutPanier.php');
        }
        
        break;
    }
    
    case 'supprimerPanier': {
        unset($_SESSION['basket'][$_GET['idProduit']]);
        header('Location: index.php?uc=basket&action=consulterPanier');
        
        break;
    }
    
    case 'consulterPanier': {
        $produits = array();
        
        //Pour chaque produit du panier, on va récupérer les informations
        foreach($_SESSION['basket'] as $productNumber => $productQuantity) {
            array_push($produits, getProductById($productNumber));
        }
        
        include('view/v_panier.php');
        
        break;
    }
    
    case 'validerPanier': {
        
        $champsPost = array();
        
        foreach($_SESSION['basket'] as $key => $produitCommande) {
            
            $lineObject = array(
                'productId' => $key,
                'quantity' => $produitCommande
            );
            
            array_push($champsPost, $lineObject);
        }
        
        $result = createCommande($champsPost);
        
        $message = "";
        if($result === '') {
            $message = "Votre commande a bien été validée ! Vous allez être redirigé vers notre catalogue";
            $_SESSION['basket'] = array();
        } else {
            $message = "Un problème est survenu lors de la validation de votre commande, veuillez réessayer ultérieurement";
        }
        
        include('view/v_actionValide.php');
        header("Refresh: 3;URL=index.php?uc=productCategories&action=voirCategories");
        
        break;
    }
}