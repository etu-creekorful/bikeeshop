﻿<?php

include('./manager/m_user.php');

if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'demandeconnexion';
}
$action = $_REQUEST['action'];
switch ($action) {
    case 'demandeConnexion': {
        include("view/v_connection.php");
        break;
    }
    case 'valideConnexion': {
        $login = $_REQUEST['login'];
        $mdp = $_REQUEST['mdp'];

        $authentification = connectUser($login, $mdp);
        if(!($authentification->token === "000")) {
            //Si connexion réussie
            $token = $authentification->token;
            registerTokenInSession($token);
            initialiserPanier();
            header('Location: index.php');
        } else {
            //En cas d'échec de connexion
            echo '<div class="alert alert-danger" role="alert"><p>Identifiant ou mot de passe incorrects</p></div>';
        }            
    }
    default : {
        include("view/v_connection.php");
        break;
    }
}
?>