<?php

include("config.inc.php");

/**
 * Fonctions pour l'application GSB
 * @package default
 * @author VK
 * @version    1.0
 */
/**
 * Teste si un quelconque visiteur est connecté
 * @return vrai ou faux 
 */
function isConnected() {
    return isset($_SESSION['token']);
}
/**
 * Enregistre dans une variable session les infos d'un visiteur
 * 
 * @param $id 
 * @param $nom
 * @param $prenom
 */
function registerTokenInSession($token) {
    $_SESSION['token'] = $token;
}
/**
 * Détruit la session active
 */
function deconnecter() {
    session_destroy();
}

/**
 * Initialise le panier
 */
function initialiserPanier() {
    if (!isset($_SESSION['basket']))
    {
        $_SESSION['basket'] = array();
    }
}

/**
* initialise un client URL
* @author
* @return le client URL
*/
function initCurl($complementURL) {
    // construire l'URL
    $url = API_URL.$complementURL;
    // initialiser la session http
    $unCurl = curl_init($url);
    // Préciser que la réponse est souhaitée
    curl_setopt($unCurl, CURLOPT_RETURNTRANSFER, true);
    // retourner le client HTTP
    return $unCurl;
}
/**
* consommer le service d'authentification
* @author
* @return le code HTTP et le jeton (ou un message d'erreur)
*/
function authAPI($login, $mdp) {
    
    $champsPost = array(
        'email' => $login,
        'password' => $mdp
    );
    
    $champsPostString = json_encode($champsPost);
    
    // initialiser le client URL
    $ch = initCurl(LOGIN);
    
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $champsPostString);
    // Préciser le Content-Type
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($champsPostString)
    ));
    
    $result = curl_exec($ch);
    
    // convertir la chaîne encodée JSON en une variable PHP
    $result = json_decode($result, false);

    // récupérer le status
    $resultStatus = curl_getinfo($ch);
    // vérifier si le jeton a été obtenu
    if ($resultStatus['http_code'] == 200) {
        // dans ce cas le retour est un objet qui expose la propriété token
        $authResponse = (object)array ('token' => $result->token);
    } else {
        // dans ce cas le retour est un objet qui expose les propriétés code et message
        $authResponse = (object)array ('token' => '000');
    }
    // fermer la session
    curl_close($ch);
    // retourner la réponse
    return $authResponse;
}


/**
* méthode GET - API Rest Frais
* @author
* @return la réponse
*/
function getAPI($complement_url) {
    // initialiser le client URL
    $unCurl = initCurl($complement_url);
    // Définir l'entête avec le jeton d'authentification
    $header = array();
    $header[] = 'Authorization: Bearer '.$_SESSION['token'];
    curl_setopt($unCurl, CURLOPT_HTTPHEADER, $header);
    // Envoyer la requête
    $reponse = curl_exec($unCurl);
    // récupérer le status, ici juste le code HTTP
    $httpCode = curl_getinfo($unCurl, CURLINFO_HTTP_CODE);
    // vérifier si il y a une réponse
    if ($httpCode == 404) {
        // false indiquera qu'il n'y a pas de ligne retournée
        $laReponse = false;
    }
    else {
        // convertir la chaîne encodée JSON en une variable PHP

        //On nettoie la chaîne JSON sinon le decodage génèrera une erreur.
        for ($i = 0; $i <= 31; ++$i) {
            $reponse = str_replace(chr($i), "", $reponse);
        }
        $reponse = str_replace(chr(127), "", $reponse);

        if (0 === strpos(bin2hex($reponse), 'efbbbf')) {
           $reponse = substr($reponse, 3);
        }
        
        $laReponse = json_decode($reponse, true);
    }
    // fermer la session
    curl_close($unCurl);
    // retourner la réponse
    return $laReponse;
}


function postAPI($complement_url, $data, $useToken = true) {
    $champsPostString = json_encode($data);
    
    $unCurl = initCurl($complement_url);
    $header = array();
    
    if($useToken === true) {
        $header[] = 'Authorization: Bearer ' . $_SESSION['token'];
    }
    
    array_push($header, 'Content-Type: application/json');
    curl_setopt($unCurl, CURLOPT_HTTPHEADER, $header);

    curl_setopt($unCurl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($unCurl, CURLOPT_POST, true);
    
    curl_setopt($unCurl, CURLOPT_CUSTOMREQUEST, "POST");

    curl_setopt($unCurl, CURLOPT_POSTFIELDS, $champsPostString);

    $result = curl_exec($unCurl);

    curl_close($unCurl);

   return $result;
}


?>