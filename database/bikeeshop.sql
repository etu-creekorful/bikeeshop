-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 06 Mai 2018 à 23:19
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bikeeshop`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `ID` int(11) NOT NULL,
  `NOM` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`ID`, `NOM`) VALUES
(1, 'Vélo enfant'),
(2, 'Vélo urbain'),
(3, 'VTT');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commande_produit`
--

CREATE TABLE `commande_produit` (
  `ID` int(11) NOT NULL,
  `COMMANDE_ID` int(10) NOT NULL,
  `PRODUIT_ID` int(10) NOT NULL,
  `QUANTITE` int(10) NOT NULL,
  `PRIX_UNITAIRE` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `ID` int(10) NOT NULL,
  `NOM` varchar(255) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `STOCK` int(10) NOT NULL,
  `PRIX` float NOT NULL,
  `TOP_PRODUIT` tinyint(1) NOT NULL DEFAULT '0',
  `CATEGORIE_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`ID`, `NOM`, `DESCRIPTION`, `STOCK`, `PRIX`, `TOP_PRODUIT`, `CATEGORIE_ID`) VALUES
(1, 'PINE MOUNTAIN 1 2018', 'Cadre : Double Butted CrMo, 27.5+ Wheels, Boost\r\n141x9mm Open Dropouts\r\nFourche : RockShox Recon RL 27.5+, 120mm Travel, compression and rebound Adjustment, alloy tapered Steerer, Solo Air Spring, 110x15mm\r\nBoost Spacing, Maxle Lite, 51mm Offset\r\nJantes :  Marin Aluminum Double Wall, 38mm Inner, pinned Joint, Disc Specific, Tubeless Compatible\r\nMoyeu arrière  :  Joytech, 141x9mm, 6-Bolt Disc, 32H\r\nMoyeu avant :  Joytech, 110x15mm, 6-Bolt Disc, 32H\r\nPneus : Vee Tire, Crown Gem 27.5x2.8", Dual\r\nControl Compound, Folding Bead, Tubeless compatible', 10, 1429, 1, 3),
(2, 'ARISE ORANGE', 'Bombtrack présente le ARISE comme ayant été conçu comme un couteau suisse : c\'est à dire adaptable à toutes les situations. \r\nVous pourrez le conserver en single speed ou le convertir avec un dérailleur . \r\nLe Arise 2016 en est à sa 3ème génération et a bénéficié des améliorations. Sa géométrie a été revue avec un tube supérieur légèrement raccourci. \r\nLe cadre est toujours en acier Chromo 4130 et le triangle avant est renforcé et heat treated pour un meilleur rapport solidité/poids.\r\n\r\nLe nouveau  sytème de frein v brake RX5 de Tektro utilise un sytème à tirage court qui permet de fonctionner avec les manettes STI. \r\nLa fourche a été rallongée pour améliorer le confort et la stabilité. Les foureaux comprennent des oeillets pour fixer un rack et des sacoches à l\'avant. \r\nLes pneus Continental Cyclocross Speed sont parfaitement polyvalents et vous donneront une très belle accroche off-road sans vous pénaliser sur le bitume. ', 15, 769.3, 2, 2),
(3, 'VTT TYGII FS 20 pouces Bleu 2017', 'Conçu pour les enfants de 6 à 8 ans, mesurant de 120 à 135 cm, le VTT Tygii 20\'\' permettra aux plus jeunes de découvrir les premières sensations en tout terrain de manière ludique et confortable. Doté d\'une géométrie sportive, ce VTT reçoit ainsi une double suspension avant et arrière pour encaisser tous les chocs, une transmission 6 vitesses pour élargir son terrain de jeu et des freins V-brake aussi simples à manipuler que fiables et efficaces.', 20, 129.99, 1, 1),
(4, 'Vélo Enfant PUKY Z8 18 pouces Gris 2017', 'Le vélo enfant Z8 créé par la marque PUKY est un modèle sûr et bien équipé, destiné aux enfants à partir de 4 ans. Bénéficiant de la très belle qualité de fabrication du constructeur allemand, c’est un vélo robuste grâce à son cadre en acier, ses jantes en alu, et ses roues, direction et pédalier à roulement à billes. L\'ensemble dispose aussi d\'une béquille pour le garer facilement. D’un accès bas, livré avec des roues de 18 pouces et doté d’un double système de freinage - par poignée réglable et adaptée aux mains des enfants pour l’avant et par rétropédalage pour l’arrière -, ce vélo enfant Z8 est idéal pour effectuer ses premières ballades en famille.', 20, 169.99, 2, 1),
(5, 'Vélo Enfant EARLY RIDER BELTER 16 pouces Argent', 'Le vélo pour enfant EARLY RIDER Belter 16 pouces est parfait pour donner ses premiers coups de pédales ! Le fabricant britannique n’a pas lésiné, et a même récolter quelques prix au passage (Eurobike Award 2012 et Red Dot Product Design Award 2013). Le cadre et la fourche sont en aluminium brossé et laqué, tandis que la tige de selle est en carbone. La transmission s’effectue par courroie, une solution légère et propre. Enfin, la sécurité est assurée par un frein arrière mini V-brake très efficace. En bref, un vélo pour rouler à toute vitesse et toute sécurité ! Et seulement 5.6 kg !', 15, 379.99, 2, 1),
(6, 'VTT Enfant COMMENCAL META HT 24+ 2018', 'Enfin un VTT Enduro Hardtail pour les plus jeunes ! Et autant le dire, le COMMENCAL Meta HT 24+ enfant tient la route, c’est du haut-de-gamme ! Il bénéficie d’une géométrie résolument All Mountain, directement inspiré des modèles Meta pour adultes. L’équipement est fiable, et performant, avec notamment des freins à disque TEKTRO M282 et une transmission 1 x 9 vitesses SRAM X9 ! Du lourd, pour les plus jeunes qui veulent enfin prendre un maximum de plaisir à piloter dans les chemins, sur du matos qui tient toutes ses promesses. Pour les riders mesurant entre 1,25 m et 1,45 m ! ', 5, 969, 2, 1),
(7, 'Vélo Enfant PUKY ZL12 ALU 12 Rose', 'Le vélo enfant PUKY ZL 12 est un modèle soigné bénéficiant de la très belle qualité de fabrication du constructeur allemand, destiné aux plus petits à partir de 3 ans. Hormis un cadre en aluminium plus léger, il est globalement identique au modèle Z2, avec un entraînement à axe carré, des jantes en alu, des roues, direction et pédalier équipés de roulements à billes. D’un accès bas et doté d’un double système de freinage - par poignée réglable et adaptée aux mains des enfants pour l’avant et par rétropédalage pour l’arrière -, le vélo Enfant PUKY ZL 12 permet de débuter le vélo en toute sécurité.', 14, 169.99, 1, 1),
(8, 'BMX SUBROSA ALTUS 16,5 pouces Vert 2018', 'Doté de roues de 16, le BMX SUBROSA Altus est adapté aux apprentis riders mesurant jusqu\'à 1,30 m. Idéal comme premier vélo à pédales, pour trouver l\'équilibre et se familiariser avec les changements de direction, il permet de découvrir en toute sécurité les joies du BMX ! Super maniable, ce mini-vélo bénéficie d\'une qualité de conception exceptionnelle, comparable à celle des BMX Subrosa pour adultes. Il est construit autour d\'un cadre et d\'une fourche solides en acier hi-ten, reçoit tout un équipement Rant performant, dont un frein arrière sécurisant. Adaptées à la taille de l\'enfant, les poignées Mini lui offrent une prise en main sûre et facile. Et la selle peut être réglée sur ses rails, au niveau de son inclinaison, pour ajuster au mieux la position de l\'enfant. ', 14, 169.99, 1, 1),
(9, 'BMX SUBROSA ALTUS 16,5 pouces Vert 2018', 'Avec ses roues de 12 pouces et son tube supérieur de 12,5 pouces, le BMX SUBROSA Altus est adapté aux apprentis riders mesurant au moins 1 mètre. Idéal comme premier vélo à pédales, pour trouver l\'équilibre et se familiariser avec les changements de direction, il permet de découvrir en toute sécurité les joies du BMX !', 15, 279.99, 2, 1),
(10, 'BMX UNITED RECRUIT JR 18,5 pouces Chrome 2018', 'Destiné aux jeunes riders, ce BMX UNITED Recruit JR 18,5 pouces est idéal pour leur permettre d\'aller plus loin dans leur découverte du BMX Freestyle. Il leur permettra de passer de plus en plus de tricks en bike park ! Dans cette version aux roues de 20 pouces, il a vraiment tout d\'un grand : très maniable, il bénéficie d\'une solidité à toute épreuve ! Son cadre et sa fourche sont conçus en acier hi-ten, tandis que ses roues affichent un rayonnage renforcé à l\'avant comme à l\'arrière (36). Ce Recruit JR intègre aussi des roulements scellés au niveau de son moyeu arrière et de son boîtier de pédalier, pour des performances durables ! ', 15, 424.99, 2, 1),
(11, 'BMX CULT JUVENILE 18 pouces Noir 2018', 'Le BMX CULT Juvenile a tous les atouts pour faire le bonheur des cyclistes en herbe ! Avec ses roues de 18 pouces, il est facile à manipuler, et très sécurisant grâce à son frein arrière. Il bénéficie surtout d\'une construction de grande qualité, autour d\'un cadre léger en aluminium. Ses pneus larges offrent un excellent confort sur la route, comme sa selle rembourrée. Tous ses composants, comme les poignées Mini, sont adaptées à la taille des petits, pour qu\'ils puissent bénéficier d\'une prise en main sûre et sécurisante.  ', 15, 439.99, 2, 1),
(12, 'BMX MANKIND NXS 18 pouces Gloss Blue ', 'Le BMX MANKIND NEXUS 18 pouces combine un Look actuel et sobre avec des composants de bonne qualité. Il convient à des jeunes Riders de 1,25 m à 1,35 m pour découvrir le plaisir du BMX. Il possède toutes les caractéristiques d\'un BMX adulte, à commencer par une excellente maniabilité ! Son cadre et sa fourche en acier Hi-ten lui confèrent une solidité à toute épreuve, tandis que les roulements scellés de son moyeu arrière, du jeu de direction intégré et de son boîtier de pédalier assurent des performances durables ! Mankind est une marque allemande concevant des produits durables et bien équipés, depuis 13 ans pour les passionnées de BMX. ', 15, 399.9, 2, 1),
(13, 'Vélo Fixie FIXIE INC FLOATER 2S RISER Blanc', 'Souvent appelée Cycle for Heroes, FIXIE Inc. est une marque créée en 2003 par deux jeunes idéalistes allemands, Recep et Holger. Les vélos ont toujours fait partie de leurs vies. Ces deux passionnés sont aux antipodes de ceux qui pensent que le vélo n\'est qu\'un simple moyen de locomotion, et ont bien compris que le vélo urbain est bien plus qu\'une simple tendance : ce marché est en train d\'exploser ! Leur créneau ? Penser plus loin que les normes, en mixant esthétisme et innovation. Le vélo fixie FIXIE INC. Floater 2S Riser est un modèle ultra léger doté d\'un moyeu intégré à deux vitesses ainsi que d\'un cintre droit très réactif. Les fixies à deux vitesses sont pensés pour les cyclistes qui pédalent souvent sur des routes avec des dénivelés conséquents : la possibilité de passer sur un second pignon facilite leurs ascensions. Avec le moyeu à deux vitesses intégrées SRAM Automatix, le passage des vitesses se fait ainsi à 12, 14, ou 18km/h.', 15, 299.99, 1, 2),
(14, 'Vélo Fixie FIXIE INC FLOATER RISER Noir', 'FIXIE Inc. a été créé en 2003 par deux jeunes idéalistes allemands, Recep et Holger. Les vélos ont toujours fait partie de leurs vies. Ces deux passionnés sont aux antipodes de ceux qui pensent que le vélo n\'est qu\'un simple moyen de locomotion, et ont bien compris que le vélo urbain est bien plus qu\'une simple tendance : ce marché est en train d\'exploser ! Leur créneau ? Penser plus loin que les normes, en mixant esthétisme et innovation. Le vélo fixie FIXIE INC. Floater Riser est un modèle ultra léger doté d\'un moyeu flip-flop et d\'un cintre riser de 600 mm, parfaits pour manœuvrer en toute sécurité en ville. Le Floater décolle dès le démarrage et garantit un freinage sans faille. Sobre et efficace !', 15, 299.99, 1, 2),
(15, 'Vélo Fixie PURE FIX CYCLES ORIGINAL PAPA Gris/Orange', 'Rendement et look à prix serré, les vélos fixie PURE FIX CYCLES de la série Original raviront les adeptes du genre au budget limité. Minimaliste, sobre et incroyablement élégant avec son cadre gris et ses jantes orange, le Papa se révélera idéal pour les déplacements urbains et le vélotaf sur terrain plat.\nComme tous les modèles Original de la marque, il est conçu autour d\'un ensemble cadre/fourche en acier efficace, d\'un cintre droit légèrement relevé offrant une position de pilotage confortable, et de superbes jantes de 45 mm de profil, donnant une touche de fun unique à ce vélo.\nPolyvalent, il reçoit un moyeu flip-flop autorisant un usage en pignon fixe ou en roue-libre et est également livré avec un frein à l\'avant.', 20, 329.99, 2, 2),
(16, 'Vélo Fixie FIXIE INC FLOATER RISER Bleu', 'FIXIE Inc. a été créé en 2003 par deux jeunes idéalistes allemands, Recep et Holger. Les vélos ont toujours fait partie de leurs vies. Ces deux passionnés sont aux antipodes de ceux qui pensent que le vélo n\'est qu\'un simple moyen de locomotion, et ont bien compris que le vélo urbain est bien plus qu\'une simple tendance : ce marché est en train d\'exploser ! Leur créneau ? Penser plus loin que les normes, en mixant esthétisme et innovation. Le vélo fixie FIXIE INC. Floater Riser est un modèle ultra léger doté d\'un moyeu flip-flop et d\'un cintre riser de 600 mm, parfaits pour manœuvrer en toute sécurité en ville. Le Floater décolle dès le démarrage et garantit un freinage sans faille. Sobre et efficace !', 20, 299.99, 2, 2),
(17, 'Vélo Fixie FIXIE INC FLOATER RISER Blanc', 'FIXIE Inc. a été créé en 2003 par deux jeunes idéalistes allemands, Recep et Holger. Les vélos ont toujours fait partie de leurs vies. Ces deux passionnés sont aux antipodes de ceux qui pensent que le vélo n\'est qu\'un simple moyen de locomotion, et ont bien compris que le vélo urbain est bien plus qu\'une simple tendance : ce marché est en train d\'exploser ! Leur créneau ? Penser plus loin que les normes, en mixant esthétisme et innovation. Le vélo fixie FIXIE INC. Floater Riser est un modèle ultra léger doté d\'un moyeu flip-flop et d\'un cintre riser de 600 mm, parfaits pour manœuvrer en toute sécurité en ville. Le Floater décolle dès le démarrage et garantit un freinage sans faille. Sobre et efficace !', 20, 299.99, 2, 2),
(18, 'Vélo Pliant TERN VERGE S8i Gris/Noir', 'Si vous rêvez d’un vélo nécessitant le moins d’entretien possible, se pliant aisément et offrant de vraies performances, le modèle TERN Verge S8i saura vous séduire ! Avec sa courroie et son moyeu à vitesse intégré, il offre un fonctionnement très peu bruyant et sans composant graisseux ce qui limite les risques de salir vos bas de pantalon. Les huit vitesses arrière garantissent un développement permettant d’attaquer facilement les montées. Les haubans allongés augmentent la stabilité, et les freins à disque hydrauliques. Il bénéficie en plus de la technologie de pliage des vélos TERN simple et rapide. Votre vélo pourra en plus rouler une fois plié pour plus de praticité lors du transport. Ce système n’est pas pour autant pénalisant, avec un design plus solide, plus sécurisant, et plus rigide de la charnière grâce à un verrouillage complet et un ajustement ultra précis. La torsion est minimale et le transfert de puissance optimal. ', 7, 1849.99, 1, 2),
(19, 'Vélo Pliant TERN LINK C8 Gris', 'Modèle d\'entrée de gamme, le vélo pliant TERN Link C8 permet de profiter du savoir-faire du spécialiste américain du vélo urbain à un tarif très accessible. Doté de la colonne de direction Physis à forge 3D, ce modèle garantit une extrême rigidité, gage de robustesse et de nervosité. Facile à utiliser et à plier grâce à la technologie N-Fold permettant de le plier en 10 secondes de manière très compacte, il roule cependant tout aussi bien qu\'un vélo de taille standard tout en garantissant une très bonne maniabilité. Ainsi, ses roues de 20 pouces le rendent facile à manoeuvrer dans la jungle urbaine tandis que les 8 vitesses bien espacées offrent une grande polyvalence, avec une plage de développement allant de 2,78 mètres pour grimper sereinement à 7,57 mètres pour aller vite lorsque vous en avez besoin. Destiné aux cyclistes mesurant de 1,42 à 1,90 m, il s\'ajuste très facilement, permettant à chacun de régler sa posture.  ', 7, 699.99, 1, 2),
(20, 'Vélo Pliant TERN LINK D8 ANDROS Blanc', 'Affichant un très bon rapport qualité/prix, le vélo pliant TERN Link D8 ravira les utilisateurs quotidiens recherchant un modèle polyvalent, fiable, et efficace en toutes circonstances. Doté de la colonne de direction Physis à forge 3D garantissant une extrême rigidité et de la potence Andros ajustable sans outil pour un ajustement de votre position de conduite à la volée, ce modèle roule tout aussi bien qu\'un vélo de taille standard. Facile à utiliser et à plier grâce à la technologie N-Fold permettant de le plier en 10 secondes de manière très compacte, il affiche une maniabilité exceptionnelle grâce à ses roues de 20 pouces qui le rendent facile à manoeuvrer dans la jungle urbaine tandis que les 8 vitesses bien espacées offrent une grande polyvalence, avec une plage de développement allant de 2,60 mètres pour grimper sereinement à 6,94 mètres pour aller vite lorsque vous en avez besoin. Destiné aux cyclistes mesurant de 1,42 à 1,90 m, il s\'ajuste très facilement, permettant à chacun de régler sa posture.', 7, 749.99, 1, 2),
(21, 'Vélo Pliant TERN LINK D8 ANDROS Rouge/Noir', 'Ce vélo pliant TERN Link D8 est un modèle citadin, compact et facilement pliable pour vous accompagner dans tous vos trajets sans limiter votre liberté ! Il bénéficie en plus de la technologie de pliage des vélos TERN simple et rapide. Votre vélo pourra en plus rouler une fois plié pour plus de praticité lors du transport. Ce système n’est pas pour autant pénalisant, avec un design plus solide, plus sécurisant, et plus rigide de la charnière grâce à un verrouillage complet et un ajustement ultra précis. L’équipement fait la part belle à la recherche du meilleur rapport qualité/prix. Avec entre autres une transmission Sram/Neos, des roues aluminium double paroi, et les poignées ergonomiques. ', 7, 749.99, 1, 2),
(22, 'Vélo Pliant TERN VERGE P10 Gris Clair', 'Destiné aux cyclistes citadins en quête d\'un modèle tout à la fois pratique et efficace, ce vélo pliant TERN Verge P10 se distingue par sa légèreté et son équipement de qualité ! Le cadre de ce vélo pliant intègre la technologie de pliage unique de TERN, rapide et sûre, qui permet de stocker sans encombre le vélo dans le coffre d\'une voiture et de le déplacer facilement. Il bénéficie aussi d\'une géométrie I-Tuned aux lignes aérodynamiques qui, tout en permettant un pliage compact, se révèle très stable à l\'usage, grâce à son empattement supérieur et à son angle de direction fermé. Ce vélo pliant est même équipé d\'une potence Syntace ajustable en hauteur et en profondeur, pour que chacun puisse trouver sa position préférentielle. Côté équipement, on note les freins à disque et la transmission 1x10 vitesses Shimano, tandis qu\'une selle Porter+ assure le confort de l\'assise.', 7, 1349.99, 2, 2),
(23, 'VTT Électrique GT BICYCLES ePANTERA DASH 27,5 pouces + Femme Bleu', 'Le GT ePantera est le VTT électrique prêt à vous accompagner au sommet lors de vos sortie dominicales. Equipé de pneus 27,5+ pour plus de confort et de facilité dans les trails techniques, sa fourche de 120 mm et son moteur Shimano seront là pour vous aider et vous faciliter la vie dans les pires ascensions. Le GT ePantera Dash est aussi équipé d\'une transmission Shimano, fiable et de freins à disque hydrauliques efficaces quelles que soient les conditions. Un VTT parfait pour débuter et aller loin dans la pratique ! ', 7, 1349.99, 1, 3),
(24, 'VTT Électrique GIANT DIRT E+ 3 27,5 pouces Bleu/Noir 2018', 'Avec superbe cadre Giant Dirt-E+ Sport 27.5 pouces Aluxx SL', 7, 1349.99, 1, 3),
(25, 'VTT MERIDA JULIET 7.40 27,5 pouces Femme Noir/Rouge 2017', 'Avec sa fourche de 100 mm de débattement et sa transmission Shimano Altus 3x9 vitesses, ce vélo spécifiquement dessiné pour s\'adapter à la morphologie des femmes vous permettra de vous aventurer sereinement sur tous les terrains, pour des balades ou des randonnées sportives tout en plaisir.\nReposant sur un cadre en aluminium doté d\'une finition soignée, ce VTT reçoit par ailleurs des freins à disque hydrauliques Tektro combinant efficacité et simplicité.', 7, 499, 1, 3),
(26, 'VTT SERIOUS EIGHT BALL 26 pouces Blanc/Bleu Pétrole', 'Le chemin de randonnée Eight Ball Trail en Californie est la piste idéale pour les vététistes débutants et pour ceux qui aiment prendre leur temps. Situé en plein coeur d\'une réserve naturelle, le Eight Ball Trail vous permet de profiter d\'un paysage aux multiples facettes : sites naturels spectaculaires, sentiers de graviers, petits chemins sinueux...', 7, 499, 1, 3),
(27, 'VTT GHOST LANAO 1 29 pouces Femme Bleu/Gris 2017 GHOST', 'Avec le VTT GHOST Lanao 1, la célèbre marque allemande propose un modèle d\'entrée de gamme spécifiquement réservé aux femmes ! Adapté à un usage de pur loisir, en balade sur des chemins de randonnée, il promet un excellent confort grâce aux 100 mm de débattement de sa suspension avant.\n\nLe cadre du Lanao 1 conçu en aluminium affiche une géométrie pensée pour les cyclistes féminines : il se caractérise notamment par un tube horizontal au sloping marqué, et par la hauteur de son poste de pilotage, qui offre une position relevée sur le vélo, très confortable ! Ce VTT Lanao 1 est présenté dans une version dotée de grandes roues 29 pouces, pour un maximum de stabilité.  \n\nCôté équipement, ce modèle pour femme intègre des freins à disque très sécurisants ! Quant à son système de transmission à 24 vitesses, signée du spécialiste japonais Shimano, il offre un large choix de braquets pour adapter son coup de pédale à la difficulté de la pente. ', 9, 499, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `produit_image`
--

CREATE TABLE `produit_image` (
  `ID` int(10) NOT NULL,
  `PRODUIT_ID` int(10) NOT NULL,
  `IMAGE_LINK` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produit_image`
--

INSERT INTO `produit_image` (`ID`, `PRODUIT_ID`, `IMAGE_LINK`) VALUES
(1, 1, 'https://www.jourdevelo.fr/3416-thickbox_default/pine-mountain-1-2018.jpg'),
(2, 2, 'https://www.jourdevelo.fr/1950-thickbox_default/arise-orange.jpg'),
(3, 3, 'https://assets.probikeshop.fr/images/products2/715/137221/137221_15012492418837.jpg'),
(4, 4, 'https://assets.probikeshop.fr/images/products2/237/128367/128367_14772934960247.jpg'),
(5, 5, 'https://assets.probikeshop.fr/images/products2/239/94415/94415-94415-1.jpg'),
(6, 6, 'https://assets.probikeshop.fr/images/products2/287/101486/101486_15054813467666.jpg'),
(7, 7, 'https://assets.probikeshop.fr/images/products2/237/103739/103739-103739.jpg'),
(8, 8, 'https://assets.probikeshop.fr/images/products2/234/140931/140931_15035777414786.jpg'),
(9, 9, 'https://assets.probikeshop.fr/images/products2/234/140930/140930_1503468725079.jpg'),
(10, 10, 'https://assets.probikeshop.fr/images/products2/200/142211/142211_15173919233988.jpg'),
(11, 11, 'https://assets.probikeshop.fr/images/products2/263/140918/140918_15053741342198.jpg'),
(12, 12, 'https://assets.probikeshop.fr/images/products2/784/149178/149178_15202379974138.jpg'),
(13, 13, 'https://images.internetstores.de/products//559220/02/81464e/FIXIE_Inc__Floater_2S_white_glossy.jpg'),
(14, 14, 'https://images.internetstores.de/products//559205/02/d3c3a3/FIXIE_Inc__Floater_black_matte.jpg'),
(15, 15, 'https://assets.probikeshop.fr/images/products2/624/123897/123897_14829401108799.jpg'),
(16, 16, 'https://images.internetstores.de/products//559211/02/82cef7/FIXIE_Inc__Floater_blue_glossy.jpg'),
(17, 17, 'https://images.internetstores.de/products//559208/02/2ea22e/FIXIE_Inc__Floater_white_glossy.jpg'),
(18, 18, 'https://assets.probikeshop.fr/images/products2/590/120445/120445_14864148546266.jpg'),
(19, 19, 'https://assets.probikeshop.fr/images/products2/590/135308/135308_14897384790647.jpg'),
(20, 20, 'https://assets.probikeshop.fr/images/products2/590/135304/135304_14876946865695.jpg'),
(21, 21, 'https://assets.probikeshop.fr/images/products2/590/120468/120468_14847352932437.jpg'),
(22, 22, 'https://assets.probikeshop.fr/images/products2/590/135301/135301_1487769268247.jpg'),
(23, 23, 'https://images.internetstores.de/products//785641/02/2777b5/GT_Bicycles_ePantera_Dash_Women_27_5___MUS.jpg'),
(25, 24, 'https://images.internetstores.de/products//696771/02/3c278b/Giant_Dirt-E__3_Black_Blue_Yellow.jpg'),
(26, 25, 'https://assets.probikeshop.fr/images/products2/658/131645/131645_14792249528368.jpg'),
(27, 26, 'https://images.internetstores.de/products//538758/02/e24f6f/Serious_Eight_Ball_Lady_26__white_petrol.jpg'),
(28, 27, 'https://assets.probikeshop.fr/images/products2/226/131601/131601_14863786241418.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `ID` int(10) NOT NULL,
  `NOM` varchar(255) NOT NULL,
  `PRENOM` varchar(255) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `TOKEN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `USER_ID` (`USER_ID`);

--
-- Index pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `COMMANDE_ID` (`COMMANDE_ID`),
  ADD KEY `PRODUIT_ID` (`PRODUIT_ID`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CATEGORIE_ID` (`CATEGORIE_ID`);

--
-- Index pour la table `produit_image`
--
ALTER TABLE `produit_image`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `PRODUIT_ID` (`PRODUIT_ID`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `produit_image`
--
ALTER TABLE `produit_image`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`);

--
-- Contraintes pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD CONSTRAINT `commande_produit_ibfk_1` FOREIGN KEY (`COMMANDE_ID`) REFERENCES `commande` (`ID`),
  ADD CONSTRAINT `commande_produit_ibfk_2` FOREIGN KEY (`PRODUIT_ID`) REFERENCES `produit` (`ID`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`CATEGORIE_ID`) REFERENCES `categorie` (`ID`);

--
-- Contraintes pour la table `produit_image`
--
ALTER TABLE `produit_image`
  ADD CONSTRAINT `produit_image_ibfk_2` FOREIGN KEY (`PRODUIT_ID`) REFERENCES `produit` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
