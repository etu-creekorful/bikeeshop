package fr.bikeeshop.manager;

import fr.bikeeshop.dto.CategorieDto;

import java.util.List;

public interface CategoryManager {

    /**
     * Get all categories available
     *
     * @return a list of categorie dto associated w/ existing categorie
     */
    List<CategorieDto> getAll();
}
