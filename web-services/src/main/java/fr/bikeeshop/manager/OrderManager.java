package fr.bikeeshop.manager;

import fr.bikeeshop.dto.order.FullOrderDto;
import fr.bikeeshop.dto.order.OrderDto;

import java.util.List;

public interface OrderManager {

    /**
     * Get all user order using provided user id
     *
     * @param userId the user id
     * @return all user orders or empty list if there is none
     */
    List<FullOrderDto> getUserOrders(int userId);

    /**
     * Create an order for the specified user
     *
     * @param userId the user id
     * @param products the products to order
     */
    void createOrder(int userId, List<OrderDto> products);
}
