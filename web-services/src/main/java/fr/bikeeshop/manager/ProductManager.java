package fr.bikeeshop.manager;

import fr.bikeeshop.dto.product.DetailedProductDto;
import fr.bikeeshop.dto.product.ProductDto;

import java.util.List;

public interface ProductManager {

    /**
     * Search for products using given criteria
     *
     * @param categoryId the product categorie id if not set get all product
     * @param onlyTopProducts should we search only for top product
     * @return a list of product dto associated with given criteria
     */
    List<ProductDto> searchProducts(Integer categoryId, Boolean onlyTopProducts);

    /**
     * Get detailed information about product using his id
     *
     * @param productId the product id
     * @return the associated information about the product
     */
    DetailedProductDto getProductById(Integer productId);
}