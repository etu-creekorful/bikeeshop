package fr.bikeeshop.manager;

import fr.bikeeshop.dto.user.UserCredentialsDto;
import fr.bikeeshop.dto.user.UserDto;
import fr.bikeeshop.pojo.User;

public interface UserManager {

    /**
     * Create an user
     *
     * @param user the user information
     * @return true if the user has been created otherwise false
     */
    boolean createUser(UserDto user);

    /**
     * Login an user using the provided credentials
     *
     * @param userCredentials the user credentials
     * @return the user auth token or null if wrong credentials
     */
    String login(UserCredentialsDto userCredentials);

    /**
     * Log out an user using his id
     * This will invalidate the user token and thus require him to login again
     *
     * @param userId the user id
     */
    void logout(int userId);

    /**
     * Get an user using his auth token
     *
     * @param token the user auth token
     * @return the associated user or null if token is invalid
     */
    User getByToken(String token);
}
