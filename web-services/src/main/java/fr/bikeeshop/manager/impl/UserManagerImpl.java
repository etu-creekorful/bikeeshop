package fr.bikeeshop.manager.impl;

import com.google.common.hash.Hashing;
import fr.bikeeshop.dao.UserDao;
import fr.bikeeshop.dto.user.UserCredentialsDto;
import fr.bikeeshop.dto.user.UserDto;
import fr.bikeeshop.manager.UserManager;
import fr.bikeeshop.pojo.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Service
public class UserManagerImpl implements UserManager {

    private UserDao userDao;

    @Autowired
    public UserManagerImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public boolean createUser(UserDto userDto) {

        // Valid user dto and Email is not already registered
        if (validateUserDto(userDto) && userDao.findByEmail(userDto.getEmail()) == null) {
            User user = new User();
            user.setNom(userDto.getNom());
            user.setPrenom(userDto.getPrenom());
            user.setEmail(userDto.getEmail());
            user.setPassword(Hashing.sha256().hashString(userDto.getPassword(), StandardCharsets.UTF_8).toString());

            userDao.save(user);
            return true;
        } else {
            throw new BadRequestException("Email address already taken.");
        }
    }

    @Override
    public String login(UserCredentialsDto userCredentials) {
        User user = userDao.findByEmailAndPassword(userCredentials.getEmail(),
                Hashing.sha256().hashString(userCredentials.getPassword(), StandardCharsets.UTF_8).toString());
        if (user == null) {
            throw new NotAuthorizedException("Invalid credentials.");
        }

        // User not null
        user.setToken(generateAuthToken());
        userDao.save(user);

        return user.getToken();
    }

    @Override
    public void logout(int userId) {
        User user = userDao.findOne(userId);

        if (user != null) {
            user.setToken(null);

            userDao.save(user);
        }
    }

    @Override
    public User getByToken(String token) {
        return userDao.findByToken(token);
    }

    public static String generateAuthToken() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private boolean validateUserDto(UserDto userDto) {
        return userDto != null && (StringUtils.isNotBlank(userDto.getNom()) &&
                StringUtils.isNotBlank(userDto.getPrenom()) &&
                StringUtils.isNotBlank(userDto.getEmail()) &&
                StringUtils.isNotBlank(userDto.getPassword()));
    }
}
