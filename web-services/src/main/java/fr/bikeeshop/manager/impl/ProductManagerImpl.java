package fr.bikeeshop.manager.impl;

import fr.bikeeshop.dao.ProduitDao;
import fr.bikeeshop.dto.product.DetailedProductDto;
import fr.bikeeshop.dto.product.ProductDto;
import fr.bikeeshop.manager.ProductManager;
import fr.bikeeshop.pojo.Produit;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductManagerImpl implements ProductManager {

    private ProduitDao produitDao;

    public ProductManagerImpl(ProduitDao produitDao) {
        this.produitDao = produitDao;
    }

    @Override
    public List<ProductDto> searchProducts(Integer categoryId, Boolean onlyTopProducts) {
        List<Produit> produits = new ArrayList<>();

        if (categoryId != null && onlyTopProducts != null) {
            produits = produitDao.findByCategorieIdAndTopProduit(categoryId, onlyTopProducts);
        } else if (categoryId != null) {
            produits = produitDao.findByCategorieId(categoryId);
        } else if (onlyTopProducts != null) {
            produits = produitDao.findByTopProduit(onlyTopProducts);
        } else {
            produits = (List<Produit>) produitDao.findAll();
        }

        // Finally convert to dto's
        return produits.stream().map(ProductDto::new).collect(Collectors.toList());
    }

    @Override
    public DetailedProductDto getProductById(Integer productId) {
        return new DetailedProductDto(produitDao.findOne(productId));
    }
}
