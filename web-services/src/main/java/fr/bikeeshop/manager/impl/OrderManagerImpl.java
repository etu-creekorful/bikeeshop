package fr.bikeeshop.manager.impl;

import fr.bikeeshop.dao.CommandeDao;
import fr.bikeeshop.dao.CommandeLineDao;
import fr.bikeeshop.dao.ProduitDao;
import fr.bikeeshop.dao.UserDao;
import fr.bikeeshop.dto.order.FullOrderDto;
import fr.bikeeshop.dto.order.OrderDto;
import fr.bikeeshop.manager.OrderManager;
import fr.bikeeshop.pojo.Commande;
import fr.bikeeshop.pojo.CommandeLine;
import fr.bikeeshop.pojo.Produit;
import fr.bikeeshop.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service
public class OrderManagerImpl implements OrderManager {

    private CommandeDao orderDao;

    private UserDao userDao;

    private ProduitDao produitDao;

    private CommandeLineDao commandeLineDao;

    @Autowired
    public OrderManagerImpl(CommandeDao orderDao, UserDao userDao, ProduitDao produitDao, CommandeLineDao commandeLineDao) {
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.produitDao = produitDao;
        this.commandeLineDao = commandeLineDao;
    }

    @Override
    public List<FullOrderDto> getUserOrders(int userId) {
        List<Commande> orders = orderDao.findByUser(userDao.findOne(userId));

        List<FullOrderDto> ordersDto = new ArrayList<>();
        for (Commande order : orders) {
            ordersDto.add(new FullOrderDto(order));
        }

        return ordersDto;
    }

    @Override
    public void createOrder(int userId, List<OrderDto> products) {
        User user = userDao.findOne(userId);

        // First of all make sure that the user exist
        if (user != null) {
            Commande commande = new Commande();
            commande.setUser(user);
            commande.setDate(new Date());
            commande.setProduits(new HashSet<>());
            orderDao.save(commande);

            for (OrderDto product : products) {
                Produit produit = produitDao.findOne(product.getProductId());

                // If the provided productId is not real, just ignore the product
                if (produit != null) {
                    CommandeLine commandeLine = new CommandeLine();
                    commandeLine.setCommande(commande);
                    commandeLine.setQuantite(product.getQuantity());
                    commandeLine.setProduit(produit);
                    commandeLine.setPrixUnitaire(produit.getPrix());

                    commande.getProduits().add(commandeLine);

                    commandeLineDao.save(commandeLine);
                    orderDao.save(commande);
                }
            }
        }
    }
}