package fr.bikeeshop.manager.impl;

import fr.bikeeshop.dao.CategorieDao;
import fr.bikeeshop.dto.CategorieDto;
import fr.bikeeshop.manager.CategoryManager;
import fr.bikeeshop.pojo.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategorieManagerImpl implements CategoryManager {

    private CategorieDao categorieDao;

    @Autowired
    public CategorieManagerImpl(CategorieDao categorieDao) {
        this.categorieDao = categorieDao;
    }

    @Override
    public List<CategorieDto> getAll() {
        List<CategorieDto> categories = new ArrayList<>();

        for (Categorie categorie : categorieDao.findAll()) {
            categories.add(new CategorieDto(categorie));
        }

        return categories;
    }
}
