package fr.bikeeshop;

import fr.bikeeshop.filter.AuthenticationFilter;
import fr.bikeeshop.rest.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("api")
public class JerseyConfiguration extends ResourceConfig {
    public JerseyConfiguration() {

    }

    @PostConstruct
    public void setup() {

        // Rest endpoint
        register(SessionResource.class);
        register(UserResource.class);
        register(CategoryResource.class);
        register(ProductResource.class);
        register(OrderResource.class);

        // Filter
        register(AuthenticationFilter.class);
    }
}