package fr.bikeeshop.dao;

import fr.bikeeshop.pojo.Produit;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ProduitDao extends CrudRepository<Produit, Integer> {

    List<Produit> findByCategorieId(Integer categorieId);

    List<Produit> findByTopProduit(Boolean topProduct);

    List<Produit> findByCategorieIdAndTopProduit(Integer categorieId, Boolean topProduct);
}
