package fr.bikeeshop.dao;

import fr.bikeeshop.pojo.Commande;
import fr.bikeeshop.pojo.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CommandeDao extends CrudRepository<Commande, Integer> {

    List<Commande> findByUser(User user);
}
