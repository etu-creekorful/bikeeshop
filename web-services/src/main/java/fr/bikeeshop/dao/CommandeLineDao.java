package fr.bikeeshop.dao;

import fr.bikeeshop.pojo.CommandeLine;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CommandeLineDao extends CrudRepository<CommandeLine, Integer> {
}
