package fr.bikeeshop.dao;

import fr.bikeeshop.pojo.Categorie;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategorieDao extends CrudRepository<Categorie, Integer> {
}
