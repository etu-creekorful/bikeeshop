package fr.bikeeshop.rest.impl;

import fr.bikeeshop.dto.TokenDto;
import fr.bikeeshop.dto.user.UserCredentialsDto;
import fr.bikeeshop.manager.UserManager;
import fr.bikeeshop.rest.SessionResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@Component
public class SessionResourceImpl implements SessionResource {

    private UserManager userManager;

    @Autowired
    public SessionResourceImpl(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public TokenDto login(UserCredentialsDto credentials) {
        return new TokenDto(userManager.login(credentials));
    }

    @Override
    public void logout(@Context SecurityContext context) {
        userManager.logout(Integer.valueOf(context.getUserPrincipal().getName()));
    }
}
