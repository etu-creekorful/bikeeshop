package fr.bikeeshop.rest.impl;

import fr.bikeeshop.dto.CategorieDto;
import fr.bikeeshop.manager.CategoryManager;
import fr.bikeeshop.rest.CategoryResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryResourceImpl implements CategoryResource {

    private CategoryManager categoryManager;

    @Autowired
    public CategoryResourceImpl(CategoryManager categoryManager) {
        this.categoryManager = categoryManager;
    }

    @Override
    public List<CategorieDto> getCategories() {
        return categoryManager.getAll();
    }
}