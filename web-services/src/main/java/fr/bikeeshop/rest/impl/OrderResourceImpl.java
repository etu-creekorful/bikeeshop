package fr.bikeeshop.rest.impl;

import fr.bikeeshop.dto.order.OrderDto;
import fr.bikeeshop.dto.order.FullOrderDto;
import fr.bikeeshop.manager.OrderManager;
import fr.bikeeshop.rest.OrderResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

@Component
public class OrderResourceImpl implements OrderResource {

    private OrderManager orderManager;

    @Autowired
    public OrderResourceImpl(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    @Override
    public List<FullOrderDto> getOrders(@Context SecurityContext securityContext) {
        return orderManager.getUserOrders(Integer.valueOf(securityContext.getUserPrincipal().getName()));
    }

    @Override
    public void createOrder(List<OrderDto> products, @Context SecurityContext securityContext) {
        orderManager.createOrder(Integer.valueOf(securityContext.getUserPrincipal().getName()), products);
    }
}
