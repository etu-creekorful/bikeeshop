package fr.bikeeshop.rest.impl;

import fr.bikeeshop.dto.user.UserDto;
import fr.bikeeshop.manager.UserManager;
import fr.bikeeshop.rest.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserResourceImpl implements UserResource {

    private UserManager userManager;

    @Autowired
    public UserResourceImpl(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public void createUser(UserDto user) {
        userManager.createUser(user);
    }
}
