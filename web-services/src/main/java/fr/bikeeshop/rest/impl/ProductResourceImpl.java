package fr.bikeeshop.rest.impl;

import fr.bikeeshop.dto.product.DetailedProductDto;
import fr.bikeeshop.dto.product.ProductDto;
import fr.bikeeshop.manager.ProductManager;
import fr.bikeeshop.rest.ProductResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductResourceImpl implements ProductResource {

    private ProductManager productManager;

    @Autowired
    public ProductResourceImpl(ProductManager productManager) {
        this.productManager = productManager;
    }

    @Override
    public List<ProductDto> getProducts(Integer categoryId, Boolean onlyTopProduct) {
        return productManager.searchProducts(categoryId, onlyTopProduct);
    }

    @Override
    public DetailedProductDto getProductById(Integer id) {
        return productManager.getProductById(id);
    }
}
