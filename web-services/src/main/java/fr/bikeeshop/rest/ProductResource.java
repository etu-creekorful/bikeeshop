package fr.bikeeshop.rest;

import fr.bikeeshop.dto.product.DetailedProductDto;
import fr.bikeeshop.dto.product.ProductDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/produits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ProductResource {

    @GET
    List<ProductDto> getProducts(@QueryParam("categorieId") Integer categorieId,
                                 @QueryParam("onlyTopProduct") Boolean onlyTopProduct);

    @GET
    @Path("/{id}")
    DetailedProductDto getProductById(@PathParam("id") Integer id);
}
