package fr.bikeeshop.rest;

import fr.bikeeshop.annotation.Secured;
import fr.bikeeshop.dto.TokenDto;
import fr.bikeeshop.dto.user.UserCredentialsDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

@Path("/session")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SessionResource {

    @POST
    TokenDto login(UserCredentialsDto credentials);

    @DELETE
    @Secured
    void logout(@Context SecurityContext context);
}
