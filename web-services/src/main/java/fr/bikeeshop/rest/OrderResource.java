package fr.bikeeshop.rest;

import fr.bikeeshop.annotation.Secured;
import fr.bikeeshop.dto.order.OrderDto;
import fr.bikeeshop.dto.order.FullOrderDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

@Path("/commandes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface OrderResource {

    @GET
    @Secured
    List<FullOrderDto> getOrders(@Context SecurityContext securityContext);

    @POST
    @Secured
    void createOrder(List<OrderDto> products, @Context SecurityContext securityContext);
}
