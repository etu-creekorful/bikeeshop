package fr.bikeeshop.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "categorie")
public class Categorie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String nom;

    @OneToMany(mappedBy = "categorie", fetch = FetchType.EAGER)
    private Set<Produit> categoriesProduits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Produit> getCategoriesProduits() {
        return categoriesProduits;
    }

    public void setCategoriesProduits(Set<Produit> categoriesProduits) {
        this.categoriesProduits = categoriesProduits;
    }
}
