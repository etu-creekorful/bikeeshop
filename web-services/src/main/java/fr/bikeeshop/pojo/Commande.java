package fr.bikeeshop.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "commande")
public class Commande implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="USER_ID", nullable=false)
    private User user;

    private Date date;

    @OneToMany(mappedBy = "commande", fetch = FetchType.EAGER)
    private Set<CommandeLine> produits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<CommandeLine> getProduits() {
        return produits;
    }

    public void setProduits(Set<CommandeLine> produits) {
        this.produits = produits;
    }
}
