package fr.bikeeshop.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "produit")
public class Produit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String nom;

    private String description;

    private int stock;

    private float prix;

    private boolean topProduit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="CATEGORIE_ID", nullable=false)
    private Categorie categorie;

    @OneToMany(mappedBy = "produit", fetch = FetchType.EAGER)
    private Set<ProduitImage> images;

    @OneToMany(mappedBy = "produit", fetch = FetchType.EAGER)
    private Set<CommandeLine> commandes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public boolean isTopProduit() {
        return topProduit;
    }

    public void setTopProduit(boolean topProduit) {
        this.topProduit = topProduit;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Set<ProduitImage> getImages() {
        return images;
    }

    public void setImages(Set<ProduitImage> images) {
        this.images = images;
    }
}
