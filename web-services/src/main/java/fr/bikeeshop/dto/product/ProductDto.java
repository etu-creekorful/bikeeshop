package fr.bikeeshop.dto.product;

import fr.bikeeshop.pojo.Produit;
import fr.bikeeshop.pojo.ProduitImage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDto {

    private int id;

    private String name;

    private float prix;

    private int stock;

    private List<String> imagesLink = new ArrayList<>();

    public ProductDto(Produit produit) {
        this.id = produit.getId();
        this.name = produit.getNom();
        this.prix = produit.getPrix();
        this.stock = produit.getStock();

        imagesLink.addAll(produit.getImages()
                .stream()
                .map(ProduitImage::getImageLink)
                .collect(Collectors.toList()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public List<String> getImagesLink() {
        return imagesLink;
    }

    public void setImagesLink(List<String> imagesLink) {
        this.imagesLink = imagesLink;
    }
}
