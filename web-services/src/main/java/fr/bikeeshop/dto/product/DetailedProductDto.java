package fr.bikeeshop.dto.product;

import fr.bikeeshop.pojo.Produit;

public class DetailedProductDto extends ProductDto {

    private String description;

    public DetailedProductDto(Produit produit) {
        super(produit);

        this.description = produit.getDescription();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
