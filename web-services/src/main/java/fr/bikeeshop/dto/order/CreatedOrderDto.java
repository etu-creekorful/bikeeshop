package fr.bikeeshop.dto.order;

import fr.bikeeshop.pojo.CommandeLine;

public class CreatedOrderDto extends OrderDto {

    private float unitPrice;

    public CreatedOrderDto() {

    }

    public CreatedOrderDto(CommandeLine commandLine) {
        this.unitPrice = commandLine.getPrixUnitaire();
        this.setQuantity(commandLine.getQuantite());
        this.setProductId(commandLine.getProduit().getId());
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }
}
