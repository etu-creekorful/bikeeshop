package fr.bikeeshop.dto.order;

import fr.bikeeshop.pojo.Commande;
import fr.bikeeshop.pojo.CommandeLine;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FullOrderDto {

    private Date orderDate;

    private List<CreatedOrderDto> products = new ArrayList<>();

    public FullOrderDto() {

    }

    public FullOrderDto(Commande commande) {
        this.orderDate = commande.getDate();

        for (CommandeLine product : commande.getProduits()) {
            this.products.add(new CreatedOrderDto(product));
        }
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<CreatedOrderDto> getProducts() {
        return products;
    }

    public void setProducts(List<CreatedOrderDto> products) {
        this.products = products;
    }
}
