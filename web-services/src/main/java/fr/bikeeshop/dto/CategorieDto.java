package fr.bikeeshop.dto;

import fr.bikeeshop.pojo.Categorie;

public class CategorieDto {

    private int id;

    private String name;


    public CategorieDto() {

    }

    public CategorieDto(Categorie categorie) {
        this.id = categorie.getId();
        this.name = categorie.getNom();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
