package fr.bikeeshop.manager.impl;

import fr.bikeeshop.dao.UserDao;
import fr.bikeeshop.dto.user.UserDto;
import fr.bikeeshop.pojo.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserManagerImplTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserManagerImpl userManager;

    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUserExistingMail() {
        String email = "test@gmail.com";

        User user = new User();
        user.setEmail(email);

        UserDto userDto = new UserDto();
        userDto.setEmail(email);
        userDto.setPassword("test");

        Mockito.when(userDao.findByEmail(email)).thenReturn(user);

        Assert.assertFalse(userManager.createUser(userDto));
    }

    @Test
    public void testCreateUserNullInformation() {
        Assert.assertFalse(userManager.createUser(null));
    }

    @Test
    public void testCreateUserEmptyFields() {
        String email = "";

        UserDto userDto = new UserDto();
        userDto.setEmail(email);
        userDto.setPassword("");

        Mockito.when(userDao.findByEmail(email)).thenReturn(null);

        Assert.assertFalse(userManager.createUser(userDto));
    }

    @Test
    public void testCreateUserNonExistingMailAddress() {
        String email = "test@gmail.com";

        UserDto userDto = new UserDto();
        userDto.setEmail(email);
        userDto.setPassword("test");
        userDto.setNom("test");
        userDto.setPassword("test");
        userDto.setPrenom("test");

        Mockito.when(userDao.findByEmail(email)).thenReturn(null);

        Assert.assertTrue(userManager.createUser(userDto));
    }

    @Test
    public void testLoginValidCredentials() {

    }

    @Test
    public void testLoginInvalidCredentials() {

    }
}